#!/bin/bash

set -e

# Set cargo linker and archiver
mkdir .cargo
cat > .cargo/config <<EOF
[target.x86_64-unknown-linux-gnu]
linker = "${CC}"
ar = "${AR}"
EOF

# Install cargo package into PREFIX
cargo install ${PKG_NAME} --vers ${PKG_VERSION} --root ${PREFIX}
rm -f ${PREFIX}/.crates.toml